﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Data.Common;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Rets.Dmql.Tests.Data;
using Rets.Dmql;

namespace DmqlLibTests
{
    [TestClass]
    public class QueryBuilderTests
    {
        /*
         * TODO for tests:
         * 
         * datetime (only date tested for now)
         * partialtime (only date tested for now)
         * time zones (NOW/TODAY)
         * 
         * string_char, e.g. (Name=AB?C)
         * what about spaces in names? e.g. (State=New J*)
         * 
         */

        DmqlQueryBuilder _queryBuilder = new DmqlQueryBuilder();

        #region numbers
        [TestMethod]
        public void TestSimpleEQNumber()
        {
            List<TestData1> data = new List<TestData1>();

            TestData1 listing1 = new TestData1() { AgentId = 99 };
            data.Add(listing1);

            TestData1 listing2 = new TestData1() { AgentId = 100 };
            data.Add(listing2);

            TestData1 listing3 = new TestData1() { AgentId = 100 };
            data.Add(listing3);

            TestData1 listing4 = new TestData1() { AgentId = 101 };
            data.Add(listing4);

            var res = _queryBuilder.Build(data.AsQueryable(), "(AgentId=100)");
            Assert.AreEqual(2, res.Count());
            Assert.AreEqual(100, res.ElementAt(0).AgentId);
            Assert.AreEqual(100, res.ElementAt(1).AgentId);
        }

        [TestMethod]
        public void TestSimpleEQNegativeNumber()
        {
            List<TestData1> data = new List<TestData1>();

            TestData1 listing1 = new TestData1() { AgentId = -99 };
            data.Add(listing1);

            TestData1 listing2 = new TestData1() { AgentId = 100 };
            data.Add(listing2);

            var res = _queryBuilder.Build(data.AsQueryable(), "(AgentId=-99)");
            Assert.AreEqual(-99, res.Single().AgentId);
        }

        [TestMethod]
        public void TestSimpleGTNumber()
        {
            List<TestData1> data = new List<TestData1>();

            TestData1 listing1 = new TestData1() { AgentId = 99 };
            data.Add(listing1);

            TestData1 listing2 = new TestData1() { AgentId = 100 };
            data.Add(listing2);

            TestData1 listing3 = new TestData1() { AgentId = 120 };
            data.Add(listing3);

            TestData1 listing4 = new TestData1() { AgentId = 125 };
            data.Add(listing4);

            var res = _queryBuilder.Build(data.AsQueryable(), "(AgentId=100+)").OrderBy(r => r.AgentId);
            Assert.AreEqual(3, res.Count());
            Assert.AreEqual(100, res.ElementAt(0).AgentId);
            Assert.AreEqual(120, res.ElementAt(1).AgentId);
            Assert.AreEqual(125, res.ElementAt(2).AgentId);
        }

        [TestMethod]
        public void TestSimpleLTNumber()
        {
            List<TestData1> data = new List<TestData1>();

            TestData1 listing1 = new TestData1() { AgentId = 99 };
            data.Add(listing1);

            TestData1 listing2 = new TestData1() { AgentId = 100 };
            data.Add(listing2);

            TestData1 listing3 = new TestData1() { AgentId = 120 };
            data.Add(listing3);

            TestData1 listing4 = new TestData1() { AgentId = 125 };
            data.Add(listing4);

            var res = _queryBuilder.Build(data.AsQueryable(), "(AgentId=100-)").OrderBy(r => r.AgentId);
            Assert.AreEqual(2, res.Count());
            Assert.AreEqual(99, res.ElementAt(0).AgentId);
            Assert.AreEqual(100, res.ElementAt(1).AgentId);
        }

        [TestMethod]
        public void TestSimpleBetweenNumber()
        {
            List<TestData1> data = new List<TestData1>();

            TestData1 listing1 = new TestData1() { AgentId = 99 };
            data.Add(listing1);

            TestData1 listing2 = new TestData1() { AgentId = 100 };
            data.Add(listing2);

            TestData1 listing3 = new TestData1() { AgentId = 120 };
            data.Add(listing3);

            TestData1 listing4 = new TestData1() { AgentId = 125 };
            data.Add(listing4);

            var res = _queryBuilder.Build(data.AsQueryable(), "(AgentId=100-120)").OrderBy(r => r.AgentId);
            Assert.AreEqual(2, res.Count());
            Assert.AreEqual(100, res.ElementAt(0).AgentId);
            Assert.AreEqual(120, res.ElementAt(1).AgentId);
        }

        [TestMethod]
        public void TestMultipleRanges()
        {
            List<TestData1> data = new List<TestData1>();

            TestData1 listing1 = new TestData1() { AgentId = 99 };
            data.Add(listing1);

            TestData1 listing2 = new TestData1() { AgentId = 105 };
            data.Add(listing2);

            TestData1 listing3 = new TestData1() { AgentId = 118 };
            data.Add(listing3);

            TestData1 listing4 = new TestData1() { AgentId = 131 };
            data.Add(listing4);

            TestData1 listing5 = new TestData1() { AgentId = 141 };
            data.Add(listing5);

            var res = _queryBuilder.Build(data.AsQueryable(), "(AgentId=100-120,135+)");
            Assert.AreEqual(3, res.Count());
            Assert.AreEqual(listing2.AgentId, res.ElementAt(0).AgentId);
            Assert.AreEqual(listing3.AgentId, res.ElementAt(1).AgentId);
            Assert.AreEqual(listing5.AgentId, res.ElementAt(2).AgentId);
        }
        #endregion

        #region dates
        [TestMethod]
        public void TestSimpleEQDate()
        {
            List<TestData1> data = new List<TestData1>();

            TestData1 listing1 = new TestData1() { ListDate = new DateTime(2017, 6, 15) };
            data.Add(listing1);

            TestData1 listing2 = new TestData1() { ListDate = new DateTime(2017, 6, 16) };
            data.Add(listing2);

            TestData1 listing3 = new TestData1() { ListDate = new DateTime(2017, 6, 17) };
            data.Add(listing3);

            TestData1 listing4 = new TestData1() { ListDate = new DateTime(2017, 6, 18) };
            data.Add(listing4);

            var res = _queryBuilder.Build(data.AsQueryable(), "(ListDate=2017-06-17)");
            Assert.AreEqual(listing3.ListDate, res.Single().ListDate);
        }

        [TestMethod]
        public void TestSimpleGTDate()
        {
            List<TestData1> data = new List<TestData1>();

            TestData1 listing1 = new TestData1() { ListDate = new DateTime(2017, 6, 15) };
            data.Add(listing1);

            TestData1 listing2 = new TestData1() { ListDate = new DateTime(2017, 6, 16) };
            data.Add(listing2);

            TestData1 listing3 = new TestData1() { ListDate = new DateTime(2017, 6, 17) };
            data.Add(listing3);

            TestData1 listing4 = new TestData1() { ListDate = new DateTime(2017, 6, 18) };
            data.Add(listing4);

            var res = _queryBuilder.Build(data.AsQueryable(), "(ListDate=2017-06-17+)").OrderBy(l => l.ListDate);
            Assert.AreEqual(2, res.Count());
            Assert.AreEqual(listing3.ListDate, res.ElementAt(0).ListDate);
            Assert.AreEqual(listing4.ListDate, res.ElementAt(1).ListDate);
        }

        [TestMethod]
        public void TestSimpleLTDate()
        {
            List<TestData1> data = new List<TestData1>();

            TestData1 listing1 = new TestData1() { ListDate = new DateTime(2017, 6, 15) };
            data.Add(listing1);

            TestData1 listing2 = new TestData1() { ListDate = new DateTime(2017, 6, 16) };
            data.Add(listing2);

            TestData1 listing3 = new TestData1() { ListDate = new DateTime(2017, 6, 17) };
            data.Add(listing3);

            TestData1 listing4 = new TestData1() { ListDate = new DateTime(2017, 6, 18) };
            data.Add(listing4);

            var res = _queryBuilder.Build(data.AsQueryable(), "(ListDate=2017-06-17-)").OrderBy(l => l.ListDate);
            Assert.AreEqual(3, res.Count());
            Assert.AreEqual(listing1.ListDate, res.ElementAt(0).ListDate);
            Assert.AreEqual(listing2.ListDate, res.ElementAt(1).ListDate);
            Assert.AreEqual(listing3.ListDate, res.ElementAt(2).ListDate);
        }

        [TestMethod]
        public void TestSimpleBetweenDate()
        {
            List<TestData1> data = new List<TestData1>();

            TestData1 listing1 = new TestData1() { ListDate = new DateTime(2017, 6, 15) };
            data.Add(listing1);

            TestData1 listing2 = new TestData1() { ListDate = new DateTime(2017, 6, 16) };
            data.Add(listing2);

            TestData1 listing3 = new TestData1() { ListDate = new DateTime(2017, 6, 17) };
            data.Add(listing3);

            TestData1 listing4 = new TestData1() { ListDate = new DateTime(2017, 6, 18) };
            data.Add(listing4);

            var res = _queryBuilder.Build(data.AsQueryable(), "(ListDate=2017-06-16-2017-06-18)").OrderBy(l => l.ListDate);
            Assert.AreEqual(3, res.Count());
            Assert.AreEqual(listing2.ListDate, res.ElementAt(0).ListDate);
            Assert.AreEqual(listing3.ListDate, res.ElementAt(1).ListDate);
            Assert.AreEqual(listing4.ListDate, res.ElementAt(2).ListDate);
        }
        #endregion

        #region strings
        [TestMethod]
        public void TestSimpleStrings()
        {
            List<TestData1> data = new List<TestData1>();

            TestData1 listing1 = new TestData1() { State = "California" };
            data.Add(listing1);

            TestData1 listing2 = new TestData1() { State = "Texas" };
            data.Add(listing2);

            TestData1 listing3 = new TestData1() { State = "New York" };
            data.Add(listing3);

            TestData1 listing4 = new TestData1() { State = "Illinois" };
            data.Add(listing4);

            var res = _queryBuilder.Build(data.AsQueryable(), "(State=Texas)");
            Assert.AreEqual(listing2.State, res.Single().State);
        }

        [TestMethod]
        public void TestStringsStartsWith()
        {
            List<TestData1> data = new List<TestData1>();

            TestData1 listing1 = new TestData1() { State = "California" };
            data.Add(listing1);

            TestData1 listing2 = new TestData1() { State = "Texas" };
            data.Add(listing2);

            TestData1 listing3 = new TestData1() { State = "New York" };
            data.Add(listing3);

            TestData1 listing4 = new TestData1() { State = "Illinois" };
            data.Add(listing4);

            var res = _queryBuilder.Build(data.AsQueryable(), "(State=Tex*)");
            Assert.AreEqual(listing2.State, res.Single().State);
        }

        [TestMethod]
        public void TestStringList()
        {
            List<TestData1> data = new List<TestData1>();

            TestData1 listing1 = new TestData1() { State = "California" };
            data.Add(listing1);

            TestData1 listing2 = new TestData1() { State = "Texas" };
            data.Add(listing2);

            TestData1 listing3 = new TestData1() { State = "New York" };
            data.Add(listing3);

            TestData1 listing4 = new TestData1() { State = "Illinois" };
            data.Add(listing4);

            var res = _queryBuilder.Build(data.AsQueryable(), "(State=Texas,California)");
            Assert.AreEqual(2, res.Count());
            Assert.AreEqual(listing1.State, res.ElementAt(0).State);
            Assert.AreEqual(listing2.State, res.ElementAt(1).State);
        }

        [TestMethod]
        public void TestStringsContains()
        {
            List<TestData1> data = new List<TestData1>();

            TestData1 listing1 = new TestData1() { State = "California" };
            data.Add(listing1);

            TestData1 listing2 = new TestData1() { State = "Texas" };
            data.Add(listing2);

            TestData1 listing3 = new TestData1() { State = "New York" };
            data.Add(listing3);

            TestData1 listing4 = new TestData1() { State = "Illinois" };
            data.Add(listing4);

            var res = _queryBuilder.Build(data.AsQueryable(), "(State=*or*)");
            Assert.AreEqual(2, res.Count());
            Assert.AreEqual(listing1.State, res.ElementAt(0).State);
            Assert.AreEqual(listing3.State, res.ElementAt(1).State);
        }

        [TestMethod]
        public void TestStringsWildard()
        {
            //This test requires a SQL Server connection since it translates the DMQL string_char query
            //into a LIKE clause which doesn't exists in in-memory collections.
            var dataSource = CreateDBDataSource();

            TestData1 listing1 = new TestData1() { State = "California" };
            dataSource.Add(listing1);

            TestData1 listing2 = new TestData1() { State = "Texas" };
            dataSource.Add(listing2);

            TestData1 listing3 = new TestData1() { State = "New York" };
            dataSource.Add(listing3);

            TestData1 listing4 = new TestData1() { State = "Illinois" };
            dataSource.Add(listing4);

            var temp = _queryBuilder.Build(dataSource.Data, "(State=Te?as)");
            var x = temp.ToString();

            var res = temp.ToArray();
            Assert.AreEqual(1, res.Count());
            Assert.AreEqual(listing2.State, res.ElementAt(0).State);
        }

        private void Temp()
        {
            List<TestData1> dummyData = new List<TestData1>();

            dummyData.Where(d => d.State == "Texas");



        }

        #endregion

        #region logical
        [TestMethod]
        public void TestNOT()
        {
            List<TestData1> data = new List<TestData1>();

            TestData1 listing1 = new TestData1() { AgentId = 99 };
            data.Add(listing1);

            TestData1 listing2 = new TestData1() { AgentId = 100 };
            data.Add(listing2);

            TestData1 listing3 = new TestData1() { AgentId = 100 };
            data.Add(listing3);

            TestData1 listing4 = new TestData1() { AgentId = 101 };
            data.Add(listing4);

            var res = _queryBuilder.Build(data.AsQueryable(), "~(AgentId=100)");
            Assert.AreEqual(2, res.Count());
            Assert.AreEqual(listing1.AgentId, res.ElementAt(0).AgentId);
            Assert.AreEqual(listing4.AgentId, res.ElementAt(1).AgentId);
        }

        [TestMethod]
        public void TestOR()
        {
            List<TestData1> data = new List<TestData1>();

            TestData1 listing1 = new TestData1() { AgentId = 99 };
            data.Add(listing1);

            TestData1 listing2 = new TestData1() { AgentId = 100 };
            data.Add(listing2);

            TestData1 listing3 = new TestData1() { AgentId = 100 };
            data.Add(listing3);

            TestData1 listing4 = new TestData1() { AgentId = 101 };
            data.Add(listing4);

            var res = _queryBuilder.Build(data.AsQueryable(), "(AgentId=99)|(AgentId=101)");
            Assert.AreEqual(2, res.Count());
            Assert.AreEqual(listing1.AgentId, res.ElementAt(0).AgentId);
            Assert.AreEqual(listing4.AgentId, res.ElementAt(1).AgentId);

            //test again with OR instead of |
            res = _queryBuilder.Build(data.AsQueryable(), "(AgentId=99)OR(AgentId=101)");
            Assert.AreEqual(2, res.Count());
            Assert.AreEqual(listing1.AgentId, res.ElementAt(0).AgentId);
            Assert.AreEqual(listing4.AgentId, res.ElementAt(1).AgentId);
        }

        [TestMethod]
        public void TestAND()
        {
            List<TestData1> data = new List<TestData1>();

            TestData1 listing1 = new TestData1() { AgentId = 99 };
            data.Add(listing1);

            TestData1 listing2 = new TestData1() { AgentId = 100 };
            data.Add(listing2);

            TestData1 listing3 = new TestData1() { AgentId = 100 };
            data.Add(listing3);

            TestData1 listing4 = new TestData1() { AgentId = 101 };
            data.Add(listing4);

            var res = _queryBuilder.Build(data.AsQueryable(), "(AgentId=99+),(AgentId=100-)");
            Assert.AreEqual(3, res.Count());
            Assert.AreEqual(listing1.AgentId, res.ElementAt(0).AgentId);
            Assert.AreEqual(listing2.AgentId, res.ElementAt(1).AgentId);
            Assert.AreEqual(listing3.AgentId, res.ElementAt(2).AgentId);

            //test again with OR instead of |
            res = _queryBuilder.Build(data.AsQueryable(), "(AgentId=99+)AND(AgentId=100-)");
            Assert.AreEqual(3, res.Count());
            Assert.AreEqual(listing1.AgentId, res.ElementAt(0).AgentId);
            Assert.AreEqual(listing2.AgentId, res.ElementAt(1).AgentId);
            Assert.AreEqual(listing3.AgentId, res.ElementAt(2).AgentId);
        }

        #endregion

        #region select list
        [TestMethod]
        public void TestSelectList()
        {
            List<TestData1> data = new List<TestData1>();

            TestData1 listing1 = new TestData1() { AgentId = 99, State = "A"};
            data.Add(listing1);

            TestData1 listing2 = new TestData1() { AgentId = 100, State = "B" };
            data.Add(listing2);

            var res = _queryBuilder.Build(data.AsQueryable(), "(AgentId=100)", "AgentId,State");

            Assert.AreEqual("{\"AgentId\":100,\"State\":\"B\"}", JsonConvert.SerializeObject(res.OfType<object>().Single()));
        }
        #endregion

        #region helpers
        private IDataSource CreateDataSource()
        {
            return new InMemoryData();
        }

        private IDataSource CreateDBDataSource()
        {
            var source = new DbDataSource();
            source.Clear();
            return source;
        }
        #endregion
    }
}
