﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rets.Dmql.Tests.Data
{
    class DbDataSource : IDataSource
    {
        private class TestContext : DbContext
        {
            public virtual DbSet<TestData1> Listings { get; set; }

            public TestContext(DbConnection connection) : base(connection, true)
            {
            }
        }

        TestContext _ctx;
        public DbDataSource()
        {
            _ctx = new TestContext(new System.Data.SqlClient.SqlConnection(@"server=.\sqlexpress;database=test; trusted_connection=true"));
        }

        public void Clear()
        {
            _ctx.Database.Delete();
        }

        public IQueryable<TestData1> Data => _ctx.Listings;

        public void Add(TestData1 data)
        {
            _ctx.Listings.Add(data);
            _ctx.SaveChanges();
        }
    }
}
