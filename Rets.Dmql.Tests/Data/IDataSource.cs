﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rets.Dmql.Tests.Data
{
    interface IDataSource
    {
        IQueryable<TestData1> Data { get; }
        void Add(TestData1 data);
    }
}
