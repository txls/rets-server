﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rets.Dmql.Tests.Data
{
    class TestData1
    {
        [Key]
        public int Id { get; set; }
        public int AgentId { get; set; }
        public DateTime ListDate { get; set; }
        public string State { get; set; }

        public TestData1()
        {
            ListDate = new DateTime(1900,1,1);
        }
    }
}
