﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rets.Dmql.Tests.Data
{
    class InMemoryData : IDataSource
    {
        List<TestData1> _data = new List<TestData1>();

        public IQueryable<TestData1> Data => _data.AsQueryable();

        public void Add(TestData1 data)
        {
            _data.Add(data);
        }
    }
}
