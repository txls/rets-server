﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Threading.Tasks;

namespace Rets.Server.Model
{
    public class RetsDbContext : DbContext
    {
        public virtual DbSet<Property> Properties { get; set; }

        //todo: extract connection string 
        public RetsDbContext() 
            : base(new System.Data.SqlClient.SqlConnection(@"server=.\sqlexpress;database=rets; trusted_connection=true"), true)
        {
        }
    }
}
