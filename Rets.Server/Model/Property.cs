﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rets.Server.Model
{
    public partial class Property
    {
        public double ID { get; set; }
        public Nullable<double> ListingKey { get; set; }
        public Nullable<double> AgentID { get; set; }
        public Nullable<double> ListAgentKey { get; set; }
        public Nullable<double> OfficeID { get; set; }
        public Nullable<double> ListOfficeKey { get; set; }
        public int ListPrice { get; set; }
        public string SalesPrice { get; set; }
        public string StandardStatus { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public Nullable<double> ZipCode { get; set; }
        public string County { get; set; }
        public string State { get; set; }
        public string StandardPropertyType { get; set; }
        public string PropertyType { get; set; }
        public string Bedrooms { get; set; }
        public string Bathrooms { get; set; }
        public string SquareFeet { get; set; }
        public string ConstructionType { get; set; }
        public string StandardLevels { get; set; }
        public string Levels { get; set; }
        public string Exterior { get; set; }
        public string Roof { get; set; }
        public string Age { get; set; }
        public string YearBuilt { get; set; }
        public Nullable<double> Fireplace { get; set; }
        public Nullable<double> Pool { get; set; }
        public string BuildingFeatures { get; set; }
        public Nullable<double> Formals { get; set; }
        public Nullable<double> Garage_Carport { get; set; }
        public Nullable<double> CentralAir_Heat { get; set; }
        public string OtherStructures { get; set; }
        public Nullable<double> Barns { get; set; }
        public Nullable<double> Pens { get; set; }
        public Nullable<double> AdditionalHouse { get; set; }
        public string HorseAmenities { get; set; }
        public Nullable<double> HorseStalls { get; set; }
        public string WaterSource { get; set; }
        public Nullable<double> PublicWater { get; set; }
        public Nullable<double> Well { get; set; }
        public string StandardSewer { get; set; }
        public Nullable<double> Sewer { get; set; }
        public Nullable<double> Septic { get; set; }
        public string RoadSurfaceType { get; set; }
        public Nullable<double> Paved { get; set; }
        public Nullable<double> Gravel { get; set; }
        public string RoadFrontageType { get; set; }
        public Nullable<double> PrivateRoad { get; set; }
        public Nullable<double> CountyRoad { get; set; }
        public Nullable<double> FarmToMarketRoad { get; set; }
        public Nullable<double> HighwayRoad { get; set; }
        public string LotFeatures { get; set; }
        public Nullable<double> TwoAcreLake { get; set; }
        public Nullable<double> Pond { get; set; }
        public Nullable<double> LiveCreek { get; set; }
        public Nullable<double> SeasonalCreek { get; set; }
        public Nullable<double> HeavilyWooded { get; set; }
        public Nullable<double> PartiallyWooded { get; set; }
        public Nullable<double> SandySoil { get; set; }
        public Nullable<double> OpenLand { get; set; }
        public Nullable<double> ImprovedPasture { get; set; }
        public string Topography { get; set; }
        public Nullable<double> Rolling { get; set; }
        public Nullable<double> MostlyFlat { get; set; }
        public Nullable<double> WaterFront { get; set; }
        public Nullable<double> MineralLease { get; set; }
        public Nullable<double> AgriculturalLease { get; set; }
        public Nullable<double> Restricted { get; set; }
        public string MaintenanceFees { get; set; }
        public string TaxExemptions { get; set; }
        public Nullable<double> AgExemption { get; set; }
        public Nullable<double> Acreage { get; set; }
        public string Subdivision { get; set; }
        public string SchoolDistrict { get; set; }
        public string RNumber { get; set; }
        public string Taxes { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string ListDate { get; set; }
        public string CloseDate { get; set; }
        public string ShowingRequirements { get; set; }
    }
}
