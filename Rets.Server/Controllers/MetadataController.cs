﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Xml.Linq;
using System.Reflection;
using System.IO;

namespace Rets.Server.Controllers
{
    public class MetadataRequest
    {
        public string Type { get; set; }
        public string Format { get; set; }
        public int Id { get; set; }
    }

    [Route("[controller]")]
    public class MetadataController : Controller
    {
        // GET api/values
        public IActionResult Get(MetadataRequest request)
        {
            string s = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("Rets.Server.metadatastandardxml.xml")).ReadToEnd();


            return Content(s, "text/xml");
        }
    }
}
