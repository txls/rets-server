﻿using Microsoft.AspNetCore.Mvc;
using Rets.Dmql;
using Rets.Server.Services;
using System.Linq;

namespace Rets.Server.Controllers
{
    public enum IncludeCountOptions
    {
        No=0,
        Yes=1,
        OnlyCount=2
    }

    public enum StandardNames
    {
        SystemNames,
        StandardNames,
        DataDictionary
    }

    public class SearchRequest
    {
        public string SearchType { get; set; }
        public string Class { get; set; }
        public IncludeCountOptions Count { get; set; }
        public string QueryType { get; set; }
        public string Limit { get; set; }
        public int Offset { get; set; }
        public string Format { get; set; }
        public string Select { get; set; }
        public string Query { get; set; }
        public string RestrictedIndicator { get; set; } = "NULL";
    }


    public class SearchController : Controller
    {
        DmqlQueryBuilder _dmqlQueryBuilder = new DmqlQueryBuilder();

        IResourceFinder _resounceFinder;
        IDmqlResultFormatterFactory _formatterFactory;

        public SearchController(IResourceFinder queryEngine, IDmqlResultFormatterFactory formatterFactory)
        {
            _resounceFinder = queryEngine;
            _formatterFactory = formatterFactory;
        }

        //todo: can we use use POST to search?
        [HttpGet, Route("[action]")]
        public IActionResult Search(SearchRequest request)
        {
            //get the collection that is the source of the data by resourceid and class (e.g. Properties, Agents, etc.)
            IQueryable source = _resounceFinder.Get(request.SearchType, request.Class);

            //build the query
            IQueryable res = _dmqlQueryBuilder.Build(source, request.Query, request.Select);

            //evaluate the query and format the results
            var resultFormatter = _formatterFactory.GetFormatter(request.Format);
            string result = resultFormatter.Format(
                res, 
                request.Count != IncludeCountOptions.No, //should include count
                request.Count != IncludeCountOptions.OnlyCount,  //should include data
                request.Offset, 
                ParseLimit(request.Limit));

            return Ok(result);
        }

        private int ParseLimit(string limit)
        {
            if (limit == "NONE" || string.IsNullOrWhiteSpace(limit))
                return -1;
            else
                return int.Parse(limit);
        }
    }
}
