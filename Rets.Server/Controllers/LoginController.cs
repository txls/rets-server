﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Rets.Server.Services;
using Rets.Server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Rets.Server.Controllers
{
    public class LoginController : Controller
    {
        const string LOGON_TIME_KEY = "LoginStart";

        IUserDataService _userDataService;
        IUrlHelper _urlHelper;
        IBillingCalculator _billingCalculator;
        public LoginController(IUserDataService userDataService, IActionContextAccessor actionContextAccessor, IUrlHelperFactory urlHelperFactory, IBillingCalculator billingCalculator)
        {
            _userDataService = userDataService;
            _urlHelper = urlHelperFactory.GetUrlHelper(actionContextAccessor.ActionContext);
            _billingCalculator = billingCalculator;
        }

        [HttpGet, HttpPost, Route("[action]")]
        public IActionResult Login()
        {
            if (!HttpContext.Session.Keys.Contains(LOGON_TIME_KEY))
            {
                HttpContext.Session.SetString(LOGON_TIME_KEY, DateTime.UtcNow.ToString("s"));
            }

            Dictionary<string, object> responseData = new Dictionary<string, object>();
            responseData["Login"] = FullUrl("~/login");
            responseData["Logout"] = FullUrl("~/logout");
            responseData["Search"] = FullUrl("~/search");
            responseData["GetMetadata"] = FullUrl("~/metadata");
            responseData["Member name"] = _userDataService.GetUserByUserName(HttpContext.User.Identity.Name).MemberName;

            return Content(
                new XElement("RETS", new XAttribute("ReplyCode", "0"), new XAttribute("ReplyText", "Operation successful"), responseData.FormatAsKeyValueBody()).ToString(SaveOptions.DisableFormatting),
                "text/xml");
        }

        private string FullUrl(string relativeUrl)
        {
            return string.Format("{0}://{1}{2}", _urlHelper.ActionContext.HttpContext.Request.Scheme, 
                _urlHelper.ActionContext.HttpContext.Request.Host.ToUriComponent(),
                _urlHelper.Content(relativeUrl));
        }

        [HttpGet, HttpPost, Route("[action]")]
        public IActionResult Logout()
        {
            DateTime logonTime = DateTime.Parse(HttpContext.Session.GetString(LOGON_TIME_KEY));

            Dictionary<string, object> responseData = new Dictionary<string, object>();
            responseData["SignOffMessage"] = "Goodbye";
            responseData["ConnectTime"] = (int)Math.Round((DateTime.UtcNow - logonTime).TotalSeconds);
            responseData["Billing"] = _billingCalculator.GetBillingInfo(logonTime, DateTime.UtcNow);

            HttpContext.Session.Remove(LOGON_TIME_KEY);

            return Content(
                new XElement(
                    "RETS"
                        , new XAttribute("ReplyCode", "0"), new XAttribute("ReplyText", "Operation successful"),
                        new XElement(
                            "RETS-RESPONSE", 
                            responseData.FormatAsKeyValueBody())
                ).ToString()
                , "text/xml");
        }
    }
}
