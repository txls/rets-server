﻿using Rets.Server.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rets.Server.Services.Implementations
{
    public class ResourceFinder : IResourceFinder
    {
        RetsDbContext _dbContext;
        public ResourceFinder(RetsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable Get(string resourceId, string @class)
        {
            switch (resourceId)
            {
                case "Property":
                    return _dbContext.Properties;
                default:
                    throw new ArgumentException();
            }
        }
    }
}
