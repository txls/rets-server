﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Rets.Server.Services.Implementations.DmqlFormatters
{
    public class CompactDmqlResultFormatter : IDmqlResultFormatter
    {
        public string Format(IQueryable results, bool includeCount, bool includeData, int offset, int limit)
        {
            var queryableResults = (IQueryable<object>)results;

            XElement result = new XElement("RETS", new XAttribute("ReplyCode", "0"), new XAttribute("ReplyText", "Operation Successful"));

            if (includeCount)
                result.Add(new XElement("COUNT", new XAttribute("Records", queryableResults.Count())));

            if (includeData)
            {

                //temp:
                //todo: Do not use fields! Use properties instead (once I fix the LinqRuntimeTypeBuilder to create properties instead of fields)
                //using fields will be an issue in case the select parameter is not specified, in which case the querey will return the original objects with properties (not fields)
                var properties = results.ElementType.GetProperties();

                result.Add(new XElement("COLUMNS", properties.Aggregate("", (a, pi) => string.Format("{0}\t{1}", a, pi.Name)) + "\t"));

                var query = queryableResults.OrderBy(x => 1).Skip(offset);//order by must be called before skip (EF rule)
                if (limit != -1)//todo: decide - should we limit MAXRESULTS? (make a distinction between no limit specified and limit="NONE"?)
                    query = query.Take(limit);

                foreach (var res in query)
                    result.Add(new XElement("DATA", properties.Aggregate("", (a, pi) => string.Format("{0}\t{1}", a, pi.GetValue(res))) + "\t"));
            }
            return result.ToString();
        }
    }
}
