﻿using Rets.Server.Services.Implementations.DmqlFormatters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rets.Server.Services.Implementations
{
    public class DmqlResultFormatterFactory : IDmqlResultFormatterFactory
    {
        public IDmqlResultFormatter GetFormatter(string name)
        {
            switch (name)
            {
                case "COMPACT":
                    return new CompactDmqlResultFormatter();
                default:
                    throw new ArgumentException("Invalid formatter!");
            }
        }
    }
}
