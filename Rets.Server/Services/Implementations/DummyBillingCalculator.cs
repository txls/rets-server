﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rets.Server.Services.Implementations
{
    public class DummyBillingCalculator : IBillingCalculator
    {
        public string GetBillingInfo(DateTime logonTime, DateTime logoffTime)
        {
            return "dummy billing info";
        }
    }
}
