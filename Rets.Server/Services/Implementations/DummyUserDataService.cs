﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Rets.Server.Model;

namespace Rets.Server.Services.Implementations
{
    public class DummyUserDataService : IUserDataService
    {
        public User GetUserByUserName(string userName)
        {
            return new User() { Id=1, UserName = userName, Password = userName, Balance = 123.45f, MemberName = "dummy user" };
        }
    }
}
