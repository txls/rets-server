﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rets.Server.Services
{
    public interface IBillingCalculator
    {
        string GetBillingInfo(DateTime logonTime, DateTime logoffTime); 
    }
}
