﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rets.Server.Services
{
    public interface IResourceFinder
    {
        IQueryable Get(string resourceId, string @class);
    }
}
