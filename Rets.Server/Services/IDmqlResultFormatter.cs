﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rets.Server.Services
{
    public interface IDmqlResultFormatter
    {
        string Format(IQueryable results, bool includeCount, bool includeData, int offset, int limit);
    }
}
