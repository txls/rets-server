﻿using Rets.Server.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rets.Server.Services
{
    public interface IUserDataService
    {
        User GetUserByUserName(string userName);
    }
}
