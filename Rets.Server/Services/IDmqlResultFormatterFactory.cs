﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rets.Server.Services
{
    public interface IDmqlResultFormatterFactory
    {
        IDmqlResultFormatter GetFormatter(string name);
    }
}
