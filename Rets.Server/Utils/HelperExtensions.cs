﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rets.Server.Utils
{
    public static class HelperExtensions
    {
        public static string ToHexString(this byte[] array)
        {
            if (array == null)
                return null;
            else
                return BitConverter.ToString(array).Replace("-", "").ToLower();
        }

        public static string FormatAsKeyValueBody(this Dictionary<string, object> values)
        {
            StringBuilder result = new StringBuilder(Environment.NewLine);
            foreach (var kvp in values)
                result.AppendFormat("{0}={1}{2}", kvp.Key, kvp.Value, Environment.NewLine);
            return result.ToString();
        }
    }
}
