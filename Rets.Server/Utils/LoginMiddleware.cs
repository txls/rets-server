﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Rets.Server.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Rets.Server.Utils
{
    public class LoginMiddleware
    {
        const string REALM = "RETS Server";
        const string LOGOUT_URI = "/logout";
        const string NONCE_SESSION_KEY = "nonce";
        const string NONCE_COUNT_SESSION_KEY = "nc";

        MD5 _md5 = MD5.Create();
        Regex _authPattern = new Regex(@"^(?'AuthKind'\w+)\s+(?'kvp'\w+\s*=\s*((\""[^\""]*\"")|\w+))(\s*,\s*(?'kvp'\w+\s*=\s*((\""[^\""]*\"")|\w+)))*\s*$");

        private readonly RequestDelegate _next;
        IUserDataService _userService;
        public LoginMiddleware(RequestDelegate next, IUserDataService userService)
        {
            _next = next;
            _userService = userService;
        }

        public async Task Invoke(HttpContext context)
        {
            var authHeader = context.Request.Headers["Authorization"].FirstOrDefault();

            var storedNonce = context.Session.GetString(NONCE_SESSION_KEY);
            HashSet<string> storedNonceCounts;
            string s = context.Session.GetString(NONCE_COUNT_SESSION_KEY);
            if (s != null)
                storedNonceCounts = JsonConvert.DeserializeObject<HashSet<string>>(s);
            else
                storedNonceCounts = new HashSet<string>();

            if (!VerifyAuthResponse(context.Request.Method, authHeader, out string userName, storedNonce, storedNonceCounts))
            {
                var nonce = CalculateNonce(context).ToHexString();

                context.Session.SetString(NONCE_SESSION_KEY, nonce);
                context.Session.SetString(NONCE_COUNT_SESSION_KEY, JsonConvert.SerializeObject(new HashSet<string>()));
                context.Response.Cookies.Append("session-id", context.Session.Id);

                context.Response.Headers["WWW-Authenticate"] = string.Format(@"Digest realm=""{0}"", qop=""auth"", nonce =""{1}""", REALM, nonce);
                context.Response.StatusCode = StatusCodes.Status401Unauthorized;
            }
            else
            {
                if (string.Equals(context.Request.Path.Value, LOGOUT_URI, StringComparison.OrdinalIgnoreCase))
                {
                    context.Session.Remove(NONCE_SESSION_KEY);
                    context.Session.Remove(NONCE_COUNT_SESSION_KEY);
                }
                else
                {
                    context.Session.SetString(NONCE_COUNT_SESSION_KEY, JsonConvert.SerializeObject(storedNonceCounts));

                    ClaimsIdentity identity = new ClaimsIdentity();
                    identity.AddClaim(new Claim(ClaimTypes.Name, userName));
                    identity.AddClaim(new Claim(ClaimTypes.Role, "User"));
                    //todo: add claims from db if needed
                    context.User = new ClaimsPrincipal(identity);
                }

                await this._next(context);
            }
        }

        private byte[] CalculateNonce(HttpContext context)
        {
            string data = string.Format("{0}{1}{2}",
                context.Connection.RemoteIpAddress.ToString(),
                DateTime.UtcNow.Ticks.ToString(),
                GetServerPrivateKey());

            return _md5.ComputeHash(Encoding.ASCII.GetBytes(data));
        }

        private bool VerifyAuthResponse(string httpMethod, string authResponse, out string username, string expectedNonce, HashSet<string> usedNonceCounts)
        {
            username = null;

            var match = _authPattern.Match(authResponse ?? "");
            if (!match.Success)
                return false;

            string authKind = match.Groups["AuthKind"].Value;
            if (authKind != "Digest")
                return false;

            var parameters = match.Groups["kvp"]
                .Captures.OfType<Capture>()
                .ToDictionary(c => c.Value.Substring(0, c.Value.IndexOf('=')).ToLower().Trim(' '), c => c.Value.Substring(c.Value.IndexOf('=') + 1).Trim(' ', '"'));

            string nonce = parameters["nonce"];
            if (nonce != expectedNonce)
                return false;

            string nonceCount = parameters["nc"];
            if (usedNonceCounts.Contains(nonceCount))
                return false;
            else
                usedNonceCounts.Add(nonceCount);

            string cnonce = parameters["cnonce"];
            string realm = parameters["realm"];
            string uri = parameters["uri"];
            string qop = parameters["qop"];
            string response = parameters["response"];

            username = parameters["username"];
            string userPassword = _userService.GetUserByUserName(username).Password;

            var ha1 = _md5.ComputeHash(Encoding.ASCII.GetBytes(string.Format("{0}:{1}:{2}", username, realm, userPassword))).ToHexString();
            var ha2 = _md5.ComputeHash(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", httpMethod, uri))).ToHexString();

            var computedResult = _md5.ComputeHash(Encoding.ASCII.GetBytes(string.Format("{0}:{1}:{2}:{3}:{4}:{5}", ha1, nonce, nonceCount, cnonce, qop, ha2))).ToHexString();

            return computedResult == response;
        }

        private string GetServerPrivateKey()
        {
            //todo: replace with RSA(?) private key
            return "[serverdummyprivatekey]";
        }
    }
}
