﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Rets.Server.Model;
using Rets.Server.Services;
using Rets.Server.Services.Implementations;
using Rets.Server.Utils;

namespace Rets.Server
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
            services.AddSession();
            services.AddDistributedMemoryCache();
            services.AddTransient<IUserDataService, DummyUserDataService>();
            services.AddTransient<RetsDbContext, RetsDbContext>();
            services.AddTransient<IResourceFinder, ResourceFinder>();
            services.AddTransient<IDmqlResultFormatterFactory, DmqlResultFormatterFactory>();
            services.AddSingleton<IBillingCalculator, DummyBillingCalculator>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            //include mandatory headers
            app.Use((context, next) =>
            {
                context.Response.Headers["RETS-Version"] = "RETS/1.5";
                context.Response.Headers["RETS-Server"] = "CalanthaRETS";
                context.Response.Headers["Cache-Control"] = "private";

                context.Response.Headers["X-RETS-Version"] = "RETS/1.5";
                context.Response.Headers["X-RETS-Server"] = "CalanthaRETS";
                
                //if X-RETS-Request-ID was specified in the request, copy it into the response
                if (context.Request.Headers.ContainsKey("X-RETS-Request-ID"))
                    context.Response.Headers["X-RETS-Request-ID"] = context.Request.Headers["X-RETS-Request-ID"];
                if (context.Request.Headers.ContainsKey("RETS-Request-ID"))
                    context.Response.Headers["RETS-Request-ID"] = context.Request.Headers["RETS-Request-ID"];

                return next();
            });

            app.UseSession();
            app.UseMiddleware<LoginMiddleware>();
            app.UseMvc();
        }
    }
}
