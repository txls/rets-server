﻿using Antlr4.Runtime.Tree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Antlr4.Runtime.Misc;
using System.Globalization;
using Antlr4.Runtime;
using System.Data.Entity.SqlServer;

namespace Rets.Dmql.Antlr
{
    class DmqlListener : dmqlBaseListener
    {
        Expression _rootPredicateExpression;
        ParameterExpression _parameterExpression;

        public Expression FilterExpression { get; private set; }

        //serves to attach the corresponding expression to each node
        public static ParseTreeProperty<Expression> AttachedExpressionProperty = new ParseTreeProperty<Expression>();

        IQueryable _source;
        public DmqlListener(IQueryable source)
        {
            _source = source;
        }

        public override void EnterStart([NotNull] dmqlParser.StartContext context)
        {
            _parameterExpression = Expression.Parameter(_source.ElementType);
        }

        public override void ExitStart([NotNull] dmqlParser.StartContext context)
        {
            _rootPredicateExpression = AttachedExpressionProperty.Get(context.search_condition());
            AttachedExpressionProperty.Put(context, _rootPredicateExpression);

            FilterExpression = Expression.Call(
                typeof(Queryable),
                "Where",
                new Type[] { _source.ElementType },
                _source.Expression,
                Expression.Lambda(_rootPredicateExpression, new ParameterExpression[] { _parameterExpression }));
        }

        public override void ExitSearch_condition([NotNull] dmqlParser.Search_conditionContext context)
        {
            Expression e = null;
            foreach (var boolElem in context.query_clause())
            {
                var boolExpr = AttachedExpressionProperty.Get(boolElem);
                if (e == null)
                    e = boolExpr;
                else
                    e = Expression.Or(e, boolExpr);
            }
            AttachedExpressionProperty.Put(context, e);
        }

        public override void ExitQuery_clause([NotNull] dmqlParser.Query_clauseContext context)
        {
            Expression e = null;
            foreach (var boolElem in context.boolean_element())
            {
                var boolExpr = AttachedExpressionProperty.Get(boolElem);
                if (e == null)
                    e = boolExpr;
                else
                    e = Expression.And(e, boolExpr);
            }
            AttachedExpressionProperty.Put(context, e);
        }

        public override void ExitBoolean_element([NotNull] dmqlParser.Boolean_elementContext context)
        {
            Expression expression = AttachedExpressionProperty.Get(context.query_element());
            if (context.not() == null)
                AttachedExpressionProperty.Put(context, expression);
            else
                AttachedExpressionProperty.Put(context, Expression.Not(expression));
        }

        public override void ExitQuery_element([NotNull] dmqlParser.Query_elementContext context)
        {
            AttachChildExpression(context);
        }

        public override void ExitSigned_number([NotNull] dmqlParser.Signed_numberContext context)
        {
            AttachedExpressionProperty.Put(context, Expression.Constant(int.Parse(context.GetText())));
        }

        public override void ExitField_criteria([NotNull] dmqlParser.Field_criteriaContext context)
        {
            AttachedExpressionProperty.Put(context, AttachedExpressionProperty.Get(context.field_value()));
        }

        public override void ExitField_value([NotNull] dmqlParser.Field_valueContext context)
        {
            AttachChildExpression(context);
        }

        public override void ExitPeriod([NotNull] dmqlParser.PeriodContext context)
        {
            AttachChildExpression(context);
        }

        public override void ExitDmqldatetime([NotNull] dmqlParser.DmqldatetimeContext context)
        {
            AttachedExpressionProperty.Put(context, Expression.Constant(DateTime.ParseExact(context.GetText(), "yyyy-MM-dd HH:MM:ss", CultureInfo.InvariantCulture)));
        }

        public override void ExitDmqldate([NotNull] dmqlParser.DmqldateContext context)
        {
            AttachedExpressionProperty.Put(context, Expression.Constant(DateTime.ParseExact(context.GetText(), "yyyy-MM-dd", CultureInfo.InvariantCulture)));
        }

        public override void ExitRange_list([NotNull] dmqlParser.Range_listContext context)
        {
            OrChildren(context, context.range());
        }

        public override void ExitField([NotNull] dmqlParser.FieldContext context)
        {
            var propertyExpression = Expression.Property(_parameterExpression, context.GetText());
            AttachedExpressionProperty.Put(context, propertyExpression);
        }

        public override void ExitEq([NotNull] dmqlParser.EqContext context)
        {
            var fieldCriteriaCtx = context
                .Parent//fieldValue
                .Parent as dmqlParser.Field_criteriaContext;

            Expression leftExpression = AttachedExpressionProperty.Get(fieldCriteriaCtx.field());
            Expression rightExpression = Expression.Convert(AttachedExpressionProperty.Get(context.children.First()), leftExpression.Type);
            Expression e = Expression.Equal(leftExpression, rightExpression);
            AttachedExpressionProperty.Put(context, e);
        }

        public override void ExitString_list([NotNull] dmqlParser.String_listContext context)
        {
            OrChildren(context, context.@string());
        }

        public override void ExitString([NotNull] dmqlParser.StringContext context)
        {
            var fieldCriteriaCtx = context
                .Parent//stringlist
                .Parent//fieldValue
                .Parent as dmqlParser.Field_criteriaContext;

            Expression e = null;
            Expression leftExpression = AttachedExpressionProperty.Get(fieldCriteriaCtx.field());

            if (context.string_char() != null)
            {
                string searchString = context.GetText().Replace("?", "_");
                Expression rightExpression = Expression.Constant(searchString);//without start and end asterisk
                var mi = typeof(SqlFunctions).GetMethod("PatIndex", new Type[] { typeof(string), typeof(string) });
                e = Expression.Equal(
                        Expression.Call(mi, rightExpression, leftExpression),
                        Expression.Convert(Expression.Constant(1), typeof(int?)));
            }
            else if (context.string_contains() != null)
            {
                string searchString = context.string_contains().GetText();
                Expression rightExpression = Expression.Constant(searchString.Substring(1, searchString.Length - 2));//without start and end asterisk
                var mi = typeof(string).GetMethod("Contains", new Type[] { typeof(string) });
                e = Expression.Call(leftExpression, mi, rightExpression);
            }
            else if (context.string_start() != null)
            {
                string searchString = context.string_start().GetText();
                Expression rightExpression = Expression.Constant(searchString.Substring(0, searchString.Length - 1));//without asterisk
                var mi = typeof(string).GetMethod("StartsWith", new Type[] { typeof(string) });
                e = Expression.Call(leftExpression, mi, rightExpression);
            }
            else if (context.string_eq() != null)
            {
                Expression rightExpression = Expression.Constant(context.string_eq().GetText());
                e = Expression.Equal(leftExpression, rightExpression);
            }
            else
                throw new InvalidOperationException();

            AttachedExpressionProperty.Put(context, e);
        }

        public override void ExitRange([NotNull] dmqlParser.RangeContext context)
        {
            var fieldCriteriaCtx = context
                .Parent//rangelist
                .Parent//fieldValue
                .Parent as dmqlParser.Field_criteriaContext;

            Expression e = null;
            Expression leftExpression = AttachedExpressionProperty.Get(fieldCriteriaCtx.field());
            if (context.greater() != null)
            {
                Expression rightExpression = Expression.Convert(AttachedExpressionProperty.Get(context.greater().children.First()), leftExpression.Type);
                e = Expression.GreaterThanOrEqual(leftExpression, rightExpression);
            }
            else if (context.less() != null)
            {
                Expression rightExpression = Expression.Convert(AttachedExpressionProperty.Get(context.less().children.First()), leftExpression.Type);
                e = Expression.LessThanOrEqual(leftExpression, rightExpression);
            }
            else if (context.between() != null)
            {
                Expression lboundExpression = Expression.Convert(AttachedExpressionProperty.Get(context.between().children.ElementAt(0)), leftExpression.Type);
                Expression uboundExpression = Expression.Convert(AttachedExpressionProperty.Get(context.between().children.ElementAt(2)), leftExpression.Type);

                e = Expression.And(
                    Expression.GreaterThanOrEqual(leftExpression, lboundExpression),
                    Expression.LessThanOrEqual(leftExpression, uboundExpression));
            }
            else
                throw new InvalidOperationException();

            AttachedExpressionProperty.Put(context, e);
        }

        #region helpers
        private void AttachChildExpression(ParserRuleContext context)
        {
            AttachedExpressionProperty.Put(context, AttachedExpressionProperty.Get(context.children.First()));
        }
        private void OrChildren(ParserRuleContext context, IEnumerable<ParserRuleContext> childrenToCombine)
        {
            Expression e = null;
            foreach (var rangeElem in childrenToCombine)
            {
                var rangeExpression = AttachedExpressionProperty.Get(rangeElem);
                if (e == null)
                    e = rangeExpression;
                else
                    e = Expression.Or(e, rangeExpression);
            }
            AttachedExpressionProperty.Put(context, e);
        }
        #endregion
    }
}