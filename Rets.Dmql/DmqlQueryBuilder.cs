﻿using Antlr4.Runtime;
using Rets.Dmql.Antlr;
using Rets.Dmql.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Rets.Dmql
{
    public class DmqlQueryBuilder
    {
        public IQueryable<T> Build<T>(IQueryable<T> source, string dmql)
        {
            var lexer = new dmqlLexer(new AntlrInputStream(dmql));
            var tokenStream = new CommonTokenStream(lexer);
            var parser = new dmqlParser(tokenStream);
            parser.RemoveErrorListeners();
            parser.RemoveParseListeners();
            var symbolListener = new DmqlListener(source);
            parser.AddParseListener(symbolListener);
            var parseTree = parser.start();

            Expression filterExpression = symbolListener.FilterExpression;
            return (IQueryable<T>)source.Provider.CreateQuery(filterExpression);
        }

        public IQueryable Build(IQueryable source, string dmql, string select)
        {
            var query = Build((IQueryable<object>)source, dmql);

            if (string.IsNullOrWhiteSpace(select))
                return query;
            else
                return SelectDynamic(query, select.Replace(" ", "").Split(','));
        }


        public static IQueryable SelectDynamic(IQueryable source, IEnumerable<string> fieldNames)
        {
            var sourceProperties = fieldNames.Select(name => source.ElementType.GetProperty(name))
                .Where(pi => pi != null)
                .ToDictionary(pi => pi.Name, p => p.PropertyType);

            Type dynamicType = LinqRuntimeTypeBuilder.GetDynamicType(sourceProperties);

            ParameterExpression sourceItem = Expression.Parameter(source.ElementType, "t");
            IEnumerable<MemberBinding> bindings = dynamicType.GetProperties().Select(p => Expression.Bind(p, Expression.Property(sourceItem, p.Name))).OfType<MemberBinding>();

            Expression selector = Expression.Lambda(Expression.MemberInit(
                Expression.New(dynamicType.GetConstructor(Type.EmptyTypes)), bindings), sourceItem);

            return source.Provider.CreateQuery(Expression.Call(typeof(Queryable), "Select", new Type[] { source.ElementType, dynamicType },
                         Expression.Constant(source), selector));
        }
    }
}
