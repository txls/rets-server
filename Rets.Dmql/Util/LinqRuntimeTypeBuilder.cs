﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Rets.Dmql.Util
{
    public static class LinqRuntimeTypeBuilder
    {
        private static AssemblyName _assemblyName = new AssemblyName() { Name = "DynamicLinqTypes" };
        private static ModuleBuilder _moduleBuilder = null;
        private static Dictionary<string, Type> _builtTypes = new Dictionary<string, Type>();

        static LinqRuntimeTypeBuilder()
        {
            _moduleBuilder = Thread.GetDomain().DefineDynamicAssembly(_assemblyName, AssemblyBuilderAccess.Run).DefineDynamicModule(_assemblyName.Name);
        }

        private static string GetTypeKey(Dictionary<string, Type> properties)
        {
            string key = string.Empty;
            foreach (var field in properties.OrderBy(f => f.Key))
                key += field.Key + ";" + field.Value.Name + ";";
            return key;
        }

        public static Type GetDynamicType(Dictionary<string, Type> properties)
        {
            if (null == properties)
                throw new ArgumentNullException("fields");
            if (0 == properties.Count)
                throw new ArgumentOutOfRangeException("fields", "fields must have at least 1 field definition");

            try
            {
                Monitor.Enter(_builtTypes);
                string className = GetTypeKey(properties);

                if (_builtTypes.ContainsKey(className))
                    return _builtTypes[className];

                TypeBuilder typeBuilder = _moduleBuilder.DefineType(className, TypeAttributes.Public | TypeAttributes.Class | TypeAttributes.Serializable);

                foreach (var propertyKvp in properties)
                {
                    var propertyName = propertyKvp.Key;
                    var typeParameter = propertyKvp.Value;
                    var fieldAttributes = FieldAttributes.Private;
                    var fieldBuilder = typeBuilder.DefineField(string.Format("<{0}>i__Field", propertyName), typeParameter, fieldAttributes);
                    var property = typeBuilder.DefineProperty(propertyName, PropertyAttributes.None, typeParameter, Type.EmptyTypes);

                    var getMethodBuilder = typeBuilder.DefineMethod(
                        name: string.Format("get_{0}", propertyName),
                        attributes: MethodAttributes.PrivateScope | MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.SpecialName,
                        callingConvention: CallingConventions.Standard | CallingConventions.HasThis,
                        returnType: typeParameter,
                        parameterTypes: Type.EmptyTypes
                    );
                    var getMethodIlGenerator = getMethodBuilder.GetILGenerator();
                    getMethodIlGenerator.Emit(OpCodes.Ldarg_0);
                    getMethodIlGenerator.Emit(OpCodes.Ldfld, fieldBuilder);
                    getMethodIlGenerator.Emit(OpCodes.Ret);
                    property.SetGetMethod(getMethodBuilder);

                    var setMethodBuilder = typeBuilder.DefineMethod(
                        name: string.Format("set_{0}", propertyName),
                        attributes: MethodAttributes.PrivateScope | MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.SpecialName,
                        callingConvention: CallingConventions.Standard | CallingConventions.HasThis,
                        returnType: null,
                        parameterTypes: new[] { typeParameter }
                    );
                    var setMethodIlGenerator = setMethodBuilder.GetILGenerator();
                    setMethodIlGenerator.Emit(OpCodes.Ldarg_0);
                    setMethodIlGenerator.Emit(OpCodes.Ldarg_1);
                    setMethodIlGenerator.Emit(OpCodes.Stfld, fieldBuilder);
                    setMethodIlGenerator.Emit(OpCodes.Ret);
                    property.SetSetMethod(setMethodBuilder);
                }

                _builtTypes[className] = typeBuilder.CreateType();

                return _builtTypes[className];
            }
            finally
            {
                Monitor.Exit(_builtTypes);
            }
        }
    }
}
